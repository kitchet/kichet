package com.lgk.kitchet.fragments;

import com.lgk.kitchet.R;
import com.lgk.kitchet.activities.ChatBoxActivity;
import com.lgk.kitchet.listener.FeedsListener;
import com.lgk.kitchet.sdk.Feeds;
import com.lgk.kitchet.util.CustomTextView;
import com.lgk.kitchet.util.GlobalVariables;
import com.lgk.kitchet.util.Helper;
import com.lgk.kitchet.widgets.BookmarkAdapter;
import com.lgk.kitchet.widgets.ChatListAdapter;
import com.lgk.kitchet.widgets.OrdersAdapter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;

public class ConfirmedOrdersFragment extends ListFragment implements OnItemClickListener,SwipeRefreshLayout.OnRefreshListener,FeedsListener {

	private SwipeRefreshLayout mSwipeRefreshLayout;

	private Feeds feedsAPI;
	private JSONArray feedsArray;
	private ListView chatList;
	OrdersAdapter chatAdapter;
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		mSwipeRefreshLayout = (SwipeRefreshLayout) this.getActivity().findViewById(R.id.swipeContainer);
		mSwipeRefreshLayout.setOnRefreshListener(this);
		chatList = getListView();

		LayoutInflater inflater = getActivity().getLayoutInflater();
		ViewGroup header = (ViewGroup)inflater.inflate(R.layout.order_list, chatList, false);
		header.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startChatFragment();

			}
		});

		chatList.addHeaderView(header, null, false);

		feedsAPI = new Feeds(this, Helper.getFromSharedPref(getActivity(), GlobalVariables.id));
		feedsAPI.fetch(Feeds.OPTION.ORDERS);


		chatAdapter = new OrdersAdapter(this.getActivity());
		setListAdapter(chatAdapter);



		chatList.setOnItemClickListener(this);
		super.onActivityCreated(savedInstanceState);
	}



	@Override
	public void onDestroyView() {
		super.onDestroyView();
		setListAdapter(null);
	}


	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		getActivity().startActivity(new Intent(getActivity(),ChatBoxActivity.class));
	}


	private void startChatFragment(){

		ChatFragment fragment2 = new ChatFragment();
		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.replace(R.id.realtabcontent, fragment2);
		fragmentTransaction.commit();
	}

	@Override
	public void onRefresh() {
		Toast.makeText(this.getActivity(), "Refresh", Toast.LENGTH_SHORT).show();
		feedsAPI.fetch(Feeds.OPTION.FAVORITES);
	}

	@Override
	public void onSuccessGetFeeds(int responseCode, String feeds) {
		Log.d("feeds success",feeds);
		mSwipeRefreshLayout.setEnabled(false);
		try {
			feedsArray = new JSONArray(feeds);
			chatAdapter.setData(feedsArray);
			chatAdapter.notifyDataSetChanged();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onFailedGetFeeds(String errorResult) {
		Log.d("feeds failed",errorResult);
		mSwipeRefreshLayout.setEnabled(false);
	}
}
