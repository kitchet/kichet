package com.lgk.kitchet.fragments;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.lgk.kitchet.R;
import com.lgk.kitchet.activities.FragmentTabs;
import com.lgk.kitchet.listener.FeedsListener;
import com.lgk.kitchet.sdk.Feeds;
import com.lgk.kitchet.util.GlobalVariables;
import com.lgk.kitchet.util.Helper;
import com.lgk.kitchet.widgets.FeedsAdapter;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class KitchetFragment extends ListFragment implements OnRefreshListener, FeedsListener {
	private SwipeRefreshLayout mSwipeRefreshLayout;
	private ListView feedList;
	private Feeds feedsAPI;
	private JSONArray feedsArray = null;
	FeedsAdapter feedsAdapter;
    TextView txtStore_name,txtStore_tagline;
    boolean isOwner = true;
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Typeface fontAwesome = Typeface.createFromAsset(getActivity().getAssets(), "fonts/fontawesome.ttf");
        mSwipeRefreshLayout = (SwipeRefreshLayout) this.getActivity().findViewById(R.id.swipeContainer);
        mSwipeRefreshLayout.setOnRefreshListener(this);    
        
        feedList = this.getListView();
        LayoutInflater inflater = this.getActivity().getLayoutInflater();
        feedList.addHeaderView((ViewGroup)inflater.inflate(R.layout.kitchet, feedList,false));
        ((TextView)this.getActivity().findViewById(R.id.txt_kitchet_leaf)).setTypeface(fontAwesome);
        ((TextView)this.getActivity().findViewById(R.id.txt_kitchet_cutlery)).setTypeface(fontAwesome);
        ((TextView)this.getActivity().findViewById(R.id.txt_kitchet_thumbs)).setTypeface(fontAwesome);
        txtStore_name = (TextView)this.getActivity().findViewById(R.id.txtKitchetName);
        txtStore_tagline = (TextView)this.getActivity().findViewById(R.id.txtKitchetDescription);

        /*get user id if sent from feeds*/
        if(GlobalVariables.kitchetData!=""){
            feedsAPI = new Feeds(this, GlobalVariables.kitchetData);
            isOwner = false;
        }

        else {
            txtStore_tagline.setText(Helper.getFromSharedPref(this.getActivity(), GlobalVariables.store_tagline));
            txtStore_name.setText(Helper.getFromSharedPref(this.getActivity(), GlobalVariables.store_name));
            feedsAPI = new Feeds(this, Helper.getFromSharedPref(this.getActivity(), GlobalVariables.id));
            isOwner = true;
        }

        
		GlobalVariables.kitchetData = "";
		
        feedsAPI.fetch(Feeds.OPTION.KITCHET);
		
		feedsAdapter = new FeedsAdapter(this.getActivity(), FeedsAdapter.Type.kitchet);
		setListAdapter(feedsAdapter);
        
		feedList.setDivider(null);
		feedList.setOnScrollListener(new AbsListView.OnScrollListener() {  
			  @Override
			  public void onScrollStateChanged(AbsListView view, int scrollState) {

			  }
			  @Override
			  public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
			    int topRowVerticalPosition = 
			      (feedList == null || feedList.getChildCount() == 0) ? 
			        0 : feedList.getChildAt(0).getTop();
			    mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
			  }
			});

		super.onCreate(savedInstanceState);
	}

	@Override
	public void onRefresh() {
        Toast.makeText(this.getActivity(), "Refresh", Toast.LENGTH_SHORT).show();
        feedsAPI.fetch(Feeds.OPTION.KITCHET);
	}

	@Override
	public void onSuccessGetFeeds(int responseCode, String feeds) {
		Log.d("feeds success",feeds);
		mSwipeRefreshLayout.setEnabled(false);
		try {
			feedsArray = new JSONArray(feeds);
           JSONObject jsonFeed = feedsArray.getJSONObject(0);

            if(isOwner){

                txtStore_tagline.setText(Helper.getFromSharedPref(this.getActivity(), GlobalVariables.store_tagline));
                txtStore_name.setText(Helper.getFromSharedPref(this.getActivity(), GlobalVariables.store_name));
            }
            else {
                txtStore_tagline.setText(jsonFeed.getString("store_tagline"));
                txtStore_name.setText(jsonFeed.getString("store_name"));
            }


            feedsAdapter.setData(feedsArray);
			feedsAdapter.notifyDataSetChanged();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        setListAdapter(null);
    }

	@Override
	public void onFailedGetFeeds(String errorResult) {
		Log.d("feeds failed",errorResult);
		mSwipeRefreshLayout.setEnabled(false);
	}
}