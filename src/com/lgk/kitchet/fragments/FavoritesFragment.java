package com.lgk.kitchet.fragments;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.lgk.kitchet.R;
import com.lgk.kitchet.activities.FragmentTabs;
import com.lgk.kitchet.listener.FeedsListener;
import com.lgk.kitchet.sdk.Feeds;
import com.lgk.kitchet.util.GlobalVariables;
import com.lgk.kitchet.util.Helper;
import com.lgk.kitchet.widgets.BookmarkAdapter;
import com.lgk.kitchet.widgets.FeedsAdapter;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class FavoritesFragment extends ListFragment implements OnRefreshListener, FeedsListener {
	private SwipeRefreshLayout mSwipeRefreshLayout;
	private ListView feedList;
	private Feeds feedsAPI;
	private JSONArray feedsArray;
	BookmarkAdapter bookAdapter = null;;
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
        mSwipeRefreshLayout = (SwipeRefreshLayout) this.getActivity().findViewById(R.id.swipeContainer);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        feedList = this.getListView();
        feedList.addHeaderView((ViewGroup)getActivity().getLayoutInflater().inflate(R.layout.favorites, feedList, false));
        
		feedsAPI = new Feeds(this, Helper.getFromSharedPref(getActivity(),GlobalVariables.id));
		feedsAPI.fetch(Feeds.OPTION.FAVORITES);
		
		bookAdapter = new BookmarkAdapter(getActivity());
		setListAdapter(bookAdapter);

		feedList.setOnScrollListener(new AbsListView.OnScrollListener() {  
			  @Override
			  public void onScrollStateChanged(AbsListView view, int scrollState) {

			  }
			  @Override
			  public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
			    int topRowVerticalPosition = 
			      (feedList == null || feedList.getChildCount() == 0) ? 
			        0 : feedList.getChildAt(0).getTop();
			    mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
			  }
			});
        //setHasOptionsMenu();
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onRefresh() {
        Toast.makeText(this.getActivity(), "Refresh", Toast.LENGTH_SHORT).show();
        feedsAPI.fetch(Feeds.OPTION.FAVORITES);
	}

	@Override
	public void onSuccessGetFeeds(int responseCode, String feeds) {
		Log.d("feeds success",feeds);
		mSwipeRefreshLayout.setEnabled(false);
		try {
			feedsArray = new JSONArray(feeds);
			bookAdapter.setData(feedsArray);
			bookAdapter.notifyDataSetChanged();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onFailedGetFeeds(String errorResult) {
		Log.d("feeds failed",errorResult);
		mSwipeRefreshLayout.setEnabled(false);
	}


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        setListAdapter(null);
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        try {
        JSONObject jsonObject = feedsArray.getJSONObject(position);

        FragmentTabs.getInstance().setFragment(4);
        GlobalVariables.kitchetData =  jsonObject.getString("kitchet_id");;
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
