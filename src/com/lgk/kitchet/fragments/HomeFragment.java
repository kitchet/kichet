package com.lgk.kitchet.fragments;

import org.json.JSONArray;
import org.json.JSONException;

import com.lgk.kitchet.R;
import com.lgk.kitchet.listener.FeedsListener;
import com.lgk.kitchet.sdk.Feeds;
import com.lgk.kitchet.widgets.FeedsAdapter;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class HomeFragment extends ListFragment implements OnRefreshListener, FeedsListener, OnItemClickListener {
	private SwipeRefreshLayout mSwipeRefreshLayout;
	private ListView feedList;
	private Feeds feedsAPI;
	private JSONArray feedsArray = null;
	FeedsAdapter feedsAdapter;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
        mSwipeRefreshLayout = (SwipeRefreshLayout) this.getActivity().findViewById(R.id.swipeContainer);
        mSwipeRefreshLayout.setOnRefreshListener(this);		
		return inflater.inflate(R.layout.feed_list, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		feedsAPI = new Feeds(this);
		feedsAPI.fetch(Feeds.OPTION.FEEDS);
		
		feedsAdapter = new FeedsAdapter(this.getActivity());
		setListAdapter(feedsAdapter);
        feedList = this.getListView();
		//feedList.setDivider();
		feedList.setOnScrollListener(new AbsListView.OnScrollListener() {  
			  @Override
			  public void onScrollStateChanged(AbsListView view, int scrollState) {

			  }
			  @Override
			  public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
			    int topRowVerticalPosition = 
			      (feedList == null || feedList.getChildCount() == 0) ? 
			        0 : feedList.getChildAt(0).getTop();
			    mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
			  }
			});

		super.onCreate(savedInstanceState);
	}

	@Override
	public void onRefresh() {
        Toast.makeText(this.getActivity(), "Refresh", Toast.LENGTH_SHORT).show();
        feedsAPI.fetch(Feeds.OPTION.FEEDS);
	}

	@Override
	public void onSuccessGetFeeds(int responseCode, String feeds) {
		mSwipeRefreshLayout.setRefreshing(false);
		Log.d("feeds success",feeds);
		try {
			feedsArray = new JSONArray(feeds);
			feedsAdapter.setData(feedsArray);
			feedsAdapter.notifyDataSetChanged();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onFailedGetFeeds(String errorResult) {
		mSwipeRefreshLayout.setRefreshing(false);
		Log.d("feeds failed",errorResult);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
		// TODO Auto-generated method stub
		
	}
}