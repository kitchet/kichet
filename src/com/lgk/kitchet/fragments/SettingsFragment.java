package com.lgk.kitchet.fragments;

import com.lgk.kitchet.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SettingsFragment extends Fragment {
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view =  inflater.inflate(R.layout.settings, container, false);
		((SwipeRefreshLayout)this.getActivity().findViewById(R.id.swipeContainer)).setEnabled(false);
		return view;
	}

}
