package com.lgk.kitchet.widgets;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.lgk.kitchet.R;
import com.lgk.kitchet.activities.FragmentTabs;
import com.lgk.kitchet.imageloader.ImageLoader;
import com.lgk.kitchet.listener.FavoriteListener;
import com.lgk.kitchet.listener.LikeListener;
import com.lgk.kitchet.listener.OrderListener;
import com.lgk.kitchet.sdk.Favorite;
import com.lgk.kitchet.sdk.Like;
import com.lgk.kitchet.sdk.Order;
import com.lgk.kitchet.util.GlobalVariables;
import com.lgk.kitchet.util.Helper;
import com.ocpsoft.pretty.time.PrettyTime;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class FeedsAdapter extends BaseAdapter implements OnClickListener, FavoriteListener, LikeListener, OrderListener {
	private Typeface fontAwesome;
	private LayoutInflater mInflater;
	private TextView btn_bookmark;
	private Context context;
	private LinearLayout yum_button;
	private LinearLayout share_button, order_button;
	private TextView feed_title, 
		feed_date, 
		feed_description, 
		feed_items_left,
		feed_distance,
		feed_time_left,
		yum_count;
	private ImageView img_feed_image;
    private ImageButton profile_thumbnail;
	public enum Type {
		feeds, kitchet
	};
	private Type _type = Type.feeds;
	private JSONArray data;
	private View clickedObject;
    ImageLoader imgLoader;
    //time formatter
    PrettyTime time_formatter = new PrettyTime();
	public FeedsAdapter(Context context) 
	{
		this.context = context;
        imgLoader = new ImageLoader(context);
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		fontAwesome = Typeface.createFromAsset(context.getAssets(), "fonts/fontawesome.ttf");
        time_formatter = new PrettyTime();

	}
	
	public FeedsAdapter(Context context, Type type)
	{
		this(context);
		this._type = type;
	}
	
	public void setType(Type type)
	{
		this._type = type;
	}
	
	public void setData(JSONArray feeds)
	{
		this.data = feeds;
	}
	
	@Override
	public int getCount() {
		if(data!=null) {
		Log.d("feeds adapter",String.valueOf(this.data.length()));
		return this.data.length();
		} else {
			return 0;
		}
	}

	@Override
	public Object getItem(int position) {

		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {

			Log.d("feed","getView");
        if(convertView == null) {


            convertView = mInflater.inflate(R.layout.feed_box, null);
        }

            btn_bookmark = (TextView) convertView.findViewById(R.id.btn_bookmark);
            TextView btn_yum = (TextView) convertView.findViewById(R.id.btn_yum);
            btn_yum.setTypeface(fontAwesome);
            TextView btn_share = (TextView) convertView.findViewById(R.id.btn_share);
            btn_share.setTypeface(fontAwesome);
            TextView txt_fire = (TextView) convertView.findViewById(R.id.txt_fire);
            txt_fire.setTypeface(fontAwesome);
            TextView txt_distance = (TextView) convertView.findViewById(R.id.txt_distance);
            txt_distance.setTypeface(fontAwesome);
            TextView txt_time = (TextView) convertView.findViewById(R.id.txt_time);
            txt_time.setTypeface(fontAwesome);
            TextView btn_options = (TextView) convertView.findViewById(R.id
                    .btn_options);
            btn_options.setTypeface(fontAwesome);
            TextView btn_order = (TextView) convertView.findViewById(R.id
                    .btn_order);
            btn_order.setTypeface(fontAwesome);

            yum_button = (LinearLayout) convertView.findViewById(R.id.yum_button);
            yum_button.setOnClickListener(this);
            share_button = (LinearLayout) convertView.findViewById(R.id.share_button);
            share_button.setOnClickListener(this);
            order_button = (LinearLayout) convertView.findViewById(R.id.order_button);
            order_button.setOnClickListener(this);
            feed_title = (TextView) convertView.findViewById(R.id.feed_title);


            if (_type == Type.feeds) {
                btn_bookmark.setTypeface(fontAwesome);
                btn_bookmark.setOnClickListener(this);
                feed_title.setOnClickListener(this);
            } else if (_type == Type.kitchet) {
                btn_bookmark.setVisibility(View.GONE);
            }

            //Data
            feed_date = (TextView) convertView.findViewById(R.id.feed_date);
            feed_description = (TextView) convertView.findViewById(R.id.feed_description);
            feed_items_left = (TextView) convertView.findViewById(R.id.feed_items_left);
            feed_distance = (TextView) convertView.findViewById(R.id.feed_distance);
            feed_time_left = (TextView) convertView.findViewById(R.id.feed_time_left);
            yum_count = (TextView) convertView.findViewById(R.id.yum_count);
            img_feed_image = (ImageView) convertView.findViewById(R.id.img_feed_image);
            profile_thumbnail = (ImageButton) convertView.findViewById(R.id.profile_thumbnail);


            try {
                JSONObject feed = data.getJSONObject(position);

                btn_bookmark.setTag(feed.getString("user_id"));
                yum_button.setTag(feed.getString("id"));
                order_button.setTag(feed.getString("id"));
                share_button.setTag(feed.getString("photo"));

                feed_title.setText(feed.getString("store_name"));
                feed_title.setTag(feed.getString("user_id"));

                feed_date.setText(formatDate(feed.getString("date_created")));

                feed_description.setText(Html.fromHtml("<p><b>" + feed.getString("menu_name")
                        + "</b> -  " + feed.getString("description") + "</p>"));
                int itemLeft = Integer.parseInt(feed.getString("total_item"));
                if(itemLeft ==0){
                    feed_items_left.setText("SOLD OUT");
                }else {
                    feed_items_left.setText(feed.getString("total_item") + " items left");
                }

                feed_distance.setText("0.5 kms away");
                computeTimeDuration(feed.getString("time_duration"));
              //  feed_time_left.setText(formatDate(feed.getString("time_duration")));
                yum_count.setText(feed.getString("total_likes") + " Yum(s)");
                imgLoader.DisplayImage(GlobalVariables.imagePath + feed.getString("photo"),
                        R.drawable.chef_placeholder, img_feed_image);

                String avatar_url = GlobalVariables.avatarUrl(feed.getString("facebook_id"));
                Log.i("Feed thumbnail", avatar_url);
                imgLoader.DisplayImage(avatar_url, R.drawable.test_nachos, profile_thumbnail);


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


		return convertView;
	}


    private void computeTimeDuration(String time_duration){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date_duration = sdf.parse(time_duration);
            long diff = date_duration.getTime() - System.currentTimeMillis();
            if(diff<0){
                feed_time_left.setText("EXPIRED");
            }
            else {
                if(TimeUnit.MILLISECONDS.toMinutes(diff) > 60)
                {
                    feed_time_left.setText(String.format("%d hr, %d min", 
                    	    TimeUnit.MILLISECONDS.toHours(diff),
                    	    TimeUnit.MILLISECONDS.toMinutes(diff) - 
                    	    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toHours(diff))
                    	));
                } else
                {
                    feed_time_left.setText(String.format("%d min, %d sec", 
                    	    TimeUnit.MILLISECONDS.toMinutes(diff),
                    	    TimeUnit.MILLISECONDS.toSeconds(diff) - 
                    	    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(diff))
                    	));	
                }

            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }



    private String formatDate(String date){

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date d = sdf.parse(date);
            String formattedDate =  time_formatter.format(d);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
       return date;
    }


	@Override
	public void onClick(View v) {
		clickedObject = v;
		switch(v.getId())
		{
		case R.id.btn_bookmark:
			Favorite fav = new Favorite(this);
			Log.d("feed user id",Helper.getFromSharedPref(context, GlobalVariables.id));
			Log.d("feed menu id", v.getTag().toString());
			fav.post(Helper.getFromSharedPref(context, GlobalVariables.id), v.getTag().toString(), "1");
			break;
		case R.id.yum_button:
			Toast.makeText(context, "Yum!", Toast.LENGTH_SHORT).show();
			Like like = new Like(this);
			like.post(Helper.getFromSharedPref(context, GlobalVariables.id), v.getTag().toString(), "1");
			break;
		case R.id.share_button:
            FragmentTabs.getInstance().facebookPost(v.getTag().toString());

			//Toast.makeText(context, "Shared!", Toast.LENGTH_SHORT).show();
			break;
		case R.id.order_button:

            showOrderDialog();
			break;
		case R.id.feed_title:
			//Toast.makeText(context, "Go to kitchet "+v.getTag().toString(), Toast.LENGTH_SHORT).show();

			FragmentTabs.getInstance().setFragment(4);
			GlobalVariables.kitchetData = v.getTag().toString();
			break;
		}

	}

	@Override
	public void onSuccessFavoriteResult(int response_code, String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onFailedFavoriteResult(String errorResult) {
		Toast.makeText(context, errorResult, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onSucessLikeResult(int response_code, String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onFailedLikeResult(String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onSucessOrderResult(int response_code, String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onFailedOrderResult(String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}




    private  void showOrderDialog( ){
        LayoutInflater li = LayoutInflater.from(context);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View promptsView = li.inflate(R.layout.prompt_dialog, null);
        // set prompts.xml to alertdialog builder
        builder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.txtOrderItems);

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }})
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {


                        String value = userInput.getText().toString();
                        if(value.equals("")){
                            Toast.makeText(context," Please Enter a value",Toast.LENGTH_SHORT).show();
                        }
                        else {
                            int total_item = Integer.parseInt(value);
                            Order order = new Order(FeedsAdapter.this);
                            order.post(Helper.getFromSharedPref(context, GlobalVariables.id), clickedObject.getTag().toString(),total_item);
                        }

                    }
                });
        builder.create();
        builder.show();

    }


}
