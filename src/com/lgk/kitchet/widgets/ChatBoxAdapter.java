package com.lgk.kitchet.widgets;

import java.util.List;

import com.lgk.kitchet.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ChatBoxAdapter extends BaseAdapter {
	Context context;
	LayoutInflater inflater;
	
	List<String> data;
	
	public ChatBoxAdapter(Context _context, List<String> _data)
	{
		this.context = _context;
		this.data = _data;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int arg0, View convertView, ViewGroup arg2) {
		if(convertView == null)
		{
			convertView = inflater.inflate(R.layout.chat_box_item, null);
			TextView txtChatItem = (TextView) convertView.findViewById(R.id.txt_chat_item);
			txtChatItem.setText(data.get(arg0));
			
		}
		return convertView;
	}

}
