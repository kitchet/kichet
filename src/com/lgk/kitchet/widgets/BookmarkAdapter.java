package com.lgk.kitchet.widgets;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.lgk.kitchet.R;
import com.lgk.kitchet.activities.FragmentTabs;
import com.lgk.kitchet.util.GlobalVariables;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class BookmarkAdapter extends BaseAdapter implements OnClickListener {
	private Context context;
	private LayoutInflater inflater;
	private Typeface fontAwesome;
	private TextView btn_message, menu_title, menu_description;
	private ImageButton profile_thumbnail;
	private JSONArray data;
	
	public BookmarkAdapter(Context context)
	{
		this.context = context;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		fontAwesome = Typeface.createFromAsset(context.getAssets(), "fonts/fontawesome.ttf");
	}
	
	public void setData(JSONArray _bookmarks)
	{
		data = _bookmarks;
	}
	
	@Override
	public int getCount() {
		if(data!=null)
			return data.length();
		else
			return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		if(convertView == null) {
            convertView = inflater.inflate(R.layout.bookmark, null);
        }
			btn_message = (TextView)convertView.findViewById(R.id.btn_message);
			btn_message.setTypeface(fontAwesome);
			btn_message.setOnClickListener(this);
            btn_message.setVisibility(View.GONE);
			menu_title = (TextView)convertView.findViewById(R.id.menu_title);
			menu_description = (TextView)convertView.findViewById(R.id.menu_description);
			profile_thumbnail = (ImageButton)convertView.findViewById(R.id.profile_thumbnail);
            menu_title.setOnClickListener(this);
            //data
            try {
				JSONObject bookmark = data.getJSONObject(position);
				menu_title.setText(bookmark.getString("store_name"));
                menu_title.setTag(bookmark.getString("kitchet_id"));
				menu_description.setText(bookmark.getString("store_tagline"));

               avatarLoad(bookmark.getString("facebook_id"),profile_thumbnail);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch(Exception e){

            }

		return convertView;
	}

    private void avatarLoad(String ID,ImageButton profile_thumbnail){


// Calculate inSampleSize
        Bitmap bitmap = null;
        URL url = null;
        try {
            url = new URL("http://graph.facebook.com/"+ID+"/picture?type=large");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                bitmap = BitmapFactory.decodeStream(in);
                profile_thumbnail.setImageBitmap(bitmap);
            }
            finally {
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch(IOException io){

        }

    }


	@Override
	public void onClick(View v) {
        FragmentTabs.getInstance().setFragment(4);
        GlobalVariables.kitchetData = v.getTag().toString();
        int id = v.getId();
        switch(id){

            case R.id.btn_message:
                Toast.makeText(context, "Go to chat", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_title:
                FragmentTabs.getInstance().setFragment(4);
                GlobalVariables.kitchetData = v.getTag().toString();
                break;



        }



		
	}

}
