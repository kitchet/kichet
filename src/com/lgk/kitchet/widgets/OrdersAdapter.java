package com.lgk.kitchet.widgets;

import com.lgk.kitchet.R;
import com.ocpsoft.pretty.time.PrettyTime;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OrdersAdapter extends BaseAdapter implements View.OnClickListener{
    Context context;
    LayoutInflater inflater;
    private JSONArray data;
    private TextView btn_message, menu_name, customer_name,txtTime;
    private ImageButton profile_thumbnail;
    //time formatter
    PrettyTime time_formatter = new PrettyTime();
    public OrdersAdapter(Context _context)
    {
        this.context = _context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(JSONArray _data){
        this.data = _data;
    }


    @Override
    public int getCount() {
        if(data!=null)
            return data.length();
        else
            return 0;
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2) {
        if(convertView == null)
        {
            convertView = inflater.inflate(R.layout.order_item, null);
        }
        txtTime =  (TextView)convertView.findViewById(R.id.txtTime);
        customer_name = (TextView)convertView.findViewById(R.id.customer_name);
        menu_name = (TextView)convertView.findViewById(R.id.menu_name);
        profile_thumbnail = (ImageButton)convertView.findViewById(R.id.profile_thumbnail);

        customer_name.setOnClickListener(this);
        //data
        try {
            JSONObject order = data.getJSONObject(position);
            Log.i("Test", order.getString("first_name"));

            customer_name.setText(order.getString("first_name")+" "+ order.getString("last_name"));
            customer_name.setTag(order.getString("menu_id"));
            menu_name.setText(order.getString("menu_name"));
            String date_ordered =formatDate(order.getString("date_created"));
            String item_ordered  = order.getString("total_item_ordered");
            txtTime.setText(date_ordered+"\n\n"+item_ordered+" item(s)");

            avatarLoad(order.getString("facebook_id"),profile_thumbnail);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch(Exception e){

        }

        return convertView;
    }


    private void avatarLoad(String ID,ImageButton profile_thumbnail){


// Calculate inSampleSize
        Bitmap bitmap = null;
        URL url = null;
        try {
            url = new URL("http://graph.facebook.com/"+ID+"/picture?type=large");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                bitmap = BitmapFactory.decodeStream(in);
                profile_thumbnail.setImageBitmap(bitmap);
            }
            finally {
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch(IOException io){

        }

    }


    @Override
    public void onClick(View v) {

    }


    private String formatDate(String date){

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date d = sdf.parse(date);
            String formattedDate =  time_formatter.format(d);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

}
