package com.lgk.kitchet.activities;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.facebook.FacebookException;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.Session.StatusCallback;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.lgk.kitchet.R;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;



public class ShareInviteActivity extends Activity {

	Button share, invite;
	int DIALOG_REQUEST_SHARE = 111;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_share_invite);

		share = (Button) findViewById(R.id.share);
		share.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				share();

			}
		});
		invite = (Button) findViewById(R.id.invite);
		invite.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				share();
			}
		});



	}

	private void share(){
		String urlToShare = "https://www.facebook.com/KichetApp?fref=ts";
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		// intent.putExtra(Intent.EXTRA_SUBJECT, "Foo bar"); // NB: has no effect!
		intent.putExtra(Intent.EXTRA_TEXT, urlToShare);


		// See if official Facebook app is found
		boolean facebookAppFound = false;
		List<ResolveInfo> matches = getPackageManager().queryIntentActivities(intent, 0);
		for (ResolveInfo info : matches) {
			if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook.katana")) {
				intent.setPackage(info.activityInfo.packageName);
				facebookAppFound = true;
				break;
			}
		}

		// As fallback, launch sharer.php in a browser
		if (!facebookAppFound) {
			String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + urlToShare;
			intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
		}
		startActivityForResult(intent, DIALOG_REQUEST_SHARE);
	}

	private void invite() {

		Intent intent = new Intent(this, FragmentTabs.class);
		startActivity(intent);
		finish();

	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {  	
		if (requestCode == DIALOG_REQUEST_SHARE) 
		{    	
			Intent intent = new Intent(this, FragmentTabs.class);
			startActivity(intent);
			finish();
		}
	}


}
