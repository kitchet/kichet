package com.lgk.kitchet.activities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.model.GraphUser;
import com.lgk.kitchet.R;
import com.lgk.kitchet.R.id;
import com.lgk.kitchet.R.layout;
import com.lgk.kitchet.R.menu;
import com.lgk.kitchet.listener.RegisterListener;
import com.lgk.kitchet.sdk.Login;
import com.lgk.kitchet.sdk.Register;
import com.lgk.kitchet.util.CustomEditText;
import com.lgk.kitchet.util.CustomTextView;
import com.lgk.kitchet.util.GlobalVariables;
import com.lgk.kitchet.util.Helper;

import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class RegisterActivity extends ActionBarActivity implements RegisterListener{

	CustomEditText firstName, lastName, emailAddress, mobile, store_name, store_tagline;
	CustomTextView terms;
	private Spinner gender, country;
	private String mGender [] = {"Male", "Female"};

	String fb_id;

	Button register_btn;

	String gender_data;

	Register register;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_register);

		register_btn = (Button) findViewById(R.id.register);
		register = new Register();
		register.setListener(this);



		terms = (CustomTextView) findViewById(R.id.terms);

		terms.setText(Html.fromHtml("<p align=center>Clicking on register means that you agree to our \n>Terms and Conditions.</p> "));
		//terms.setMovementMethod(LinkMovementMethod.getInstance());
		terms.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				loadTerms();
				
				
			}
		});

		firstName = (CustomEditText) findViewById(R.id.firstName);
		lastName = (CustomEditText) findViewById(R.id.lastName);
		emailAddress = (CustomEditText) findViewById(R.id.emailAddress);
		mobile = (CustomEditText) findViewById(R.id.mobile);
		gender = (Spinner) findViewById(R.id.gender);
		country = (Spinner) findViewById(R.id.country);
		
		store_name = (CustomEditText) findViewById(R.id.store_name);
		store_tagline = (CustomEditText) findViewById(R.id.store_tagline);

		register_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				register();



			}
		});



		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, mGender);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		gender.setAdapter(dataAdapter);

		gender.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {

				gender_data = gender.getSelectedItem().toString();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

		ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, getCountries());
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		country.setAdapter(dataAdapter2);

		country.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {


			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

		Bundle extras = getIntent().getExtras();
        try {
            if (extras != null){

                firstName.setText(extras.getCharSequence("first_name"));
                lastName.setText(extras.getCharSequence("last_name"));
                emailAddress.setText(extras.getCharSequence("email"));
                fb_id = extras.getString("fb_id");

                if(extras.getCharSequence("gender")!=null) {
                    if (gender != null) {
                        if (extras.getCharSequence("gender").equals("Male"))
                            gender.setSelection(0);
                        else
                            gender.setSelection(1);

                    }
                }
                else {
                    gender.setSelection(1);
                }




            }
        }catch(Exception e){

        }



	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.register, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private ArrayList<String> getCountries(){

		ArrayList<String> countries = new ArrayList<String>();
		Locale[] locales = Locale.getAvailableLocales();

		for (Locale locale : locales) {
			String country = locale.getDisplayCountry();
			if (country.trim().length()>0 && !countries.contains(country)) {
				countries.add(country);
			}

			Collections.sort(countries, new Comparator<String>() {
				@Override
				public int compare(String s1, String s2) {
					return s1.compareToIgnoreCase(s2);
				}
			});
		}

		return countries;
	}

	private void register(){

		if( (Helper.textValidator(RegisterActivity.this, firstName)) && (Helper.textValidator(RegisterActivity.this, lastName)) && (Helper.textValidator(RegisterActivity.this, emailAddress)) && (Helper.textValidator(RegisterActivity.this, mobile))  ){
			Helper.showProcessingDialog(RegisterActivity.this, "Registration", "Please wait...");
			try {
				register.register(fb_id, firstName.getText().toString(), lastName.getText().toString(),
                        emailAddress.getText().toString(), mobile.getText().toString(),
                        gender.getSelectedItem().toString(),
                        country.getSelectedItem().toString(), "Makati",store_name.getText().toString(),
                        store_tagline.getText().toString());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else{
			Helper.showDialog(RegisterActivity.this, "Please check your input. All fields are required.");
		}

	}

	@Override
	public void onSuccessRegisterResult(int responseCode, JSONObject result, String message) {
		// TODO Auto-generated method stub
		Helper.dismissDialog();
		if(responseCode == 1){
			Helper.saveOnSharedPref(RegisterActivity.this, GlobalVariables.isLogin, "true");
			try {
				Helper.saveOnSharedPref(RegisterActivity.this, GlobalVariables.id, result.getString("id"));
                Helper.saveOnSharedPref(RegisterActivity.this, GlobalVariables.store_name, result.getString("store_name"));
                Helper.saveOnSharedPref(RegisterActivity.this, GlobalVariables.store_tagline, result.getString("store_tagline"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Toast.makeText(RegisterActivity.this, "Successfully registered!", Toast.LENGTH_SHORT).show();
			Intent intent = new Intent(RegisterActivity.this, ShareInviteActivity.class);
			startActivity(intent);
			finish();
		}
		else{
			Helper.showDialog(RegisterActivity.this, "Regsitration Error");
		}


	}

	@Override
	public void onFailedRegisterResult(String errorResult) {
		Helper.dismissDialog();
		// TODO Auto-generated method stub
		Helper.showDialog(RegisterActivity.this, "Regsitration Error: " + errorResult);

	}
	
	private void loadTerms(){
		Helper.showDialog(RegisterActivity.this, getString(R.string.termss));
	}
}
