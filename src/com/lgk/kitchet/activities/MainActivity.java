package com.lgk.kitchet.activities;

import java.util.Arrays;

import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.*;
import com.facebook.model.*;
import com.facebook.Request;
import com.facebook.Session;
import com.facebook.model.GraphUser;
import com.lgk.kitchet.R;
import com.lgk.kitchet.fragments.HomeFragment;
import com.lgk.kitchet.listener.LoginListener;
import com.lgk.kitchet.sdk.Feeds;
import com.lgk.kitchet.sdk.Login;
import com.lgk.kitchet.util.GlobalVariables;
import com.lgk.kitchet.util.Helper;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;


public class MainActivity extends ActionBarActivity implements LoginListener{

	Button login_with_facebook;
	GraphUser _user;

	Login login;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);

			login = new Login();
			login.setListener(this);

	
		
		

		login_with_facebook = (Button) findViewById(R.id.facebook_login);
		login_with_facebook.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub]
				getFBInfo();

			}
		});
	}



	@SuppressWarnings("static-access")
	private void getFBInfo(){
		Session s = new Session(this);

		s.openForRead(new Session.OpenRequest(this).setPermissions(Arrays.asList("email")));

		// start Facebook Login
		s.openActiveSession(this, true, new Session.StatusCallback() {

			// callback when session changes state
			@SuppressWarnings("deprecation")
			@Override
			public void call(Session session, SessionState state, Exception exception) {
				if (session.isOpened()) {

					// make request to the /me API
					Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {
						// callback after Graph API response with user object
						@Override
						public void onCompleted(GraphUser user, Response response) {
							if (user != null) {
								_user = user;
								try {
									Helper.showProcessingDialog(MainActivity.this, "Loading", "Please wait...");
									login.login(user.getId());

								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							else{
								Helper.showDialog(MainActivity.this, "Cannot connect to Facebook");
							}
						}
					});
					session.closeAndClearTokenInformation();
				} else
				{
					Log.d("Facebook.session","not opened");
				}
			}
		});
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
	}

	@Override
	public void onSuccessLoginResult(int responseCode, JSONObject result, String message) {
		// TODO Auto-generated method stub
		//not yet registered
		Helper.dismissDialog();
		Log.d("RESPONSE CODE LOGIN", result+"");
		if(responseCode == 0){
			Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
			try {
				intent.putExtra("fb_id", _user.getId());
				intent.putExtra("first_name", _user.getFirstName());
				intent.putExtra("last_name", _user.getLastName());
				intent.putExtra("email", _user.getInnerJSONObject().getString("email"));
				intent.putExtra("gender", _user.getInnerJSONObject().getString("gender"));
				
			

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			startActivity(intent);

		}
		else{
			Helper.saveOnSharedPref(MainActivity.this, GlobalVariables.isLogin, "true");
			try {
				Helper.saveOnSharedPref(MainActivity.this, GlobalVariables.id, result.getString("id"));
                Helper.saveOnSharedPref(MainActivity.this, GlobalVariables.store_name, result.getString("store_name"));
                Helper.saveOnSharedPref(MainActivity.this, GlobalVariables.store_tagline, result.getString("store_tagline"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Intent intent = new Intent(MainActivity.this, FragmentTabs.class);
			startActivity(intent);
		}
		finish();
	}

	@Override
	public void onFailedLoginResult(String result) {
		// TODO Auto-generated method stub
		Helper.dismissDialog();
		Helper.showDialog(this, "Unable to get user information");
		Log.d("ERROR", result);
	}
}
