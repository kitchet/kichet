package com.lgk.kitchet.activities;

import java.util.ArrayList;
import java.util.List;

import com.lgk.kitchet.R;
import com.lgk.kitchet.util.Helper;
import com.lgk.kitchet.widgets.ChatBoxAdapter;

import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ChatBoxActivity extends ListActivity {
	TextView txt_chat_send, btn_chat_order;
	EditText txt_chat_input;

	List<String> chat_list;
	ChatBoxAdapter chatBoxAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		getActionBar().setTitle("");
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setLogo(R.drawable.ic_launcher);




		chat_list = new ArrayList<String>();
		chat_list.add("Hi, can I get 2 orders please :) ");
		chatBoxAdapter= new ChatBoxAdapter(this, chat_list);


		this.setContentView(R.layout.chat_box);
		txt_chat_input = (EditText) findViewById(R.id.txt_chat_input);
		Typeface fontawesome = Typeface.createFromAsset(getAssets(), "fonts/fontawesome.ttf");
		txt_chat_send = (TextView)findViewById(R.id.txt_chat_send);
		txt_chat_send.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				chat_list.add(txt_chat_input.getText().toString());
				chatBoxAdapter.notifyDataSetChanged();
				Helper.hideKeyboard(ChatBoxActivity.this);
				txt_chat_input.setText("");
				getListView().smoothScrollToPosition(chatBoxAdapter.getCount());

			}
		});
		txt_chat_send.setTypeface(fontawesome);
		btn_chat_order = (TextView)findViewById(R.id.btn_chat_order);
		btn_chat_order.setTypeface(fontawesome);

		ListView chat = getListView();
		ViewGroup feedbox = (ViewGroup)getLayoutInflater().inflate(R.layout.feed_box, chat, false);
		((RelativeLayout)feedbox.findViewById(R.id.feed_header)).setVisibility(View.GONE);

		TextView btn_yum = (TextView)feedbox.findViewById(R.id.btn_yum);
		btn_yum.setTypeface(fontawesome);
		TextView btn_share = (TextView)feedbox.findViewById(R.id.btn_share);
		btn_share.setTypeface(fontawesome);
		TextView txt_fire = (TextView)feedbox.findViewById(R.id.txt_fire);
		txt_fire.setTypeface(fontawesome);
		TextView txt_distance = (TextView)feedbox.findViewById(R.id.txt_distance);
		txt_distance.setTypeface(fontawesome);
		TextView txt_time = (TextView)feedbox.findViewById(R.id.txt_time);
		txt_time.setTypeface(fontawesome);
		TextView btn_options = (TextView)feedbox.findViewById(R.id.btn_options);
		btn_options.setTypeface(fontawesome);
        TextView btn_order = (TextView) feedbox.findViewById(R.id.btn_order);
        btn_order.setTypeface(fontawesome);


		chat.addHeaderView(feedbox);


		setListAdapter(chatBoxAdapter);


	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// go to previous screen when app icon in action bar is clicked
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
