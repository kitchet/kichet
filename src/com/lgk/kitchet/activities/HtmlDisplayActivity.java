package com.lgk.kitchet.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.lgk.kitchet.R;


public class HtmlDisplayActivity extends ActionBarActivity {

	private WebView mWebView;
	
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_htmldisplay);
		getSupportActionBar().hide();
		this.mWebView = (WebView)findViewById(R.id.webView);
	    this.mWebView.getSettings().setJavaScriptEnabled(true);
	    
	    this.mWebView.setBackgroundColor(0);
	    this.mWebView.setWebViewClient(new Client());
	    
	    
	    String url = getIntent().getStringExtra("url");
	    
	    if(url!=null){
	    	this.mWebView.loadUrl(getIntent().getStringExtra("url"));
	    }
	    String content = getIntent().getStringExtra("content");


	}
	
	

	
	
	private class Client extends WebViewClient{
	    private Client(){
	    }

	    public void onPageFinished(WebView paramWebView, String paramString){
	    	super.onPageFinished(paramWebView, paramString);
	    	//  HtmlDisplayActivity.this.mProgressBar.setVisibility(8);
	    }

	    public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString){
	    	Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse(paramString));
	    	HtmlDisplayActivity.this.startActivity(localIntent);
	    	return true;
	    }
	}
}

