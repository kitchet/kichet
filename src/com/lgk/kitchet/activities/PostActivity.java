package com.lgk.kitchet.activities;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.json.JSONException;
import org.json.JSONObject;

import com.lgk.kitchet.R;
import com.lgk.kitchet.listener.PostListener;
import com.lgk.kitchet.sdk.Post;
import com.lgk.kitchet.util.CustomTextView;
import com.lgk.kitchet.util.GlobalVariables;
import com.lgk.kitchet.util.Helper;

import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.AvoidXfermode.Mode;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class PostActivity extends Activity implements PostListener{

	private  static final int CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE = 1777;
	private  static final int SELECT_PHOTO = 111;
	private ImageView thumbnail;
	private EditText itemName;
	private EditText description;
	private EditText price;
	private Spinner stores;
	private EditText item;
	private Spinner duration;
	private RadioGroup mode;
	private RadioButton delivery;
	private RadioButton pickUp;
	private RadioButton meetUp;
	private EditText tags;
	private ImageView confirm;

	private String itemName_txt;
	private String description_txt;
	private String price_txt;
	private String stores_txt;
	private String item_txt;
	private String duration_txt;
	private String mode_txt;
	private String tags_txt;

	LinearLayout choosePhoto;
	private CustomTextView camera, gallery;

	String filePath = "";
	String fileName = "";

	Post post;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_post);
		getActionBar().setTitle("");
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setLogo(R.drawable.ic_launcher);
		
		dispatchTakePictureIntent();

		initFields();

		post = new Post();
		post.setListener(this);


		confirm.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				confirm();

			}
		});

		gallery.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				showGallery();


			}
		});

		camera.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dispatchTakePictureIntent();

			}
		});
		
	}

	private void dispatchTakePictureIntent() {
		fileName = Helper.getFromSharedPref(PostActivity.this, "id")+System.currentTimeMillis()+".jpg";
		Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
		File file = new File(Environment.getExternalStorageDirectory()+File.separator + fileName);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
		intent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		startActivityForResult(intent, CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {  	
		if (requestCode == CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) 
		{    	
			File file = new File(Environment.getExternalStorageDirectory()+File.separator + fileName);
			try {
				filePath = file.getAbsolutePath();
				thumbnail.setImageBitmap(imageOreintationValidator(decodeFile(file), file.getAbsolutePath()));

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (requestCode == SELECT_PHOTO && resultCode == RESULT_OK) {
			Uri selectedImage = data.getData();
			try {
				Bitmap a = decodeUri(selectedImage);
				filePath = getPath(selectedImage);
				thumbnail.setImageBitmap(imageOreintationValidator(a, getPath(selectedImage)));


			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else{
			//choosePhoto.setVisibility(View.VISIBLE);
		}

	}

	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}



	private Bitmap imageOreintationValidator(Bitmap bitmap, String path) {
		ExifInterface ei;
		try {
			ei = new ExifInterface(path);
			int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);
			switch (orientation) {
			case ExifInterface.ORIENTATION_ROTATE_90:
				bitmap = rotateImage(bitmap, 90);
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				bitmap = rotateImage(bitmap, 180);
				break;
			case ExifInterface.ORIENTATION_ROTATE_270:
				bitmap = rotateImage(bitmap, 270);
				break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return bitmap;
	}

	private Bitmap decodeFile(File f) throws IOException{
		Bitmap b = null;

		//Decode image size
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;

		FileInputStream fis = new FileInputStream(f);
		BitmapFactory.decodeStream(fis, null, o);
		fis.close();

		int scale = 1;
		if (o.outHeight > 700 || o.outWidth > 1000) {
			scale = (int)Math.pow(2, (int) Math.ceil(Math.log(1000 / 
					(double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
		}

		//Decode with inSampleSize
		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		fis = new FileInputStream(f);
		b = BitmapFactory.decodeStream(fis, null, o2);
		fis.close();

		return b;
	}


	private Bitmap rotateImage(Bitmap source, float angle) {
		Bitmap bitmap = null;
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		try {
			bitmap = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
					matrix, true);
		} catch (OutOfMemoryError err) {
			err.printStackTrace();
		}
		return bitmap;
	}


	private void confirm(){

         int duration_int[]=   getResources().getIntArray(R.array.duration_int);
		itemName_txt = itemName.getText().toString();
		description_txt = description.getText().toString();
		price_txt = price.getText().toString();
		stores_txt = stores.getSelectedItem().toString();
		item_txt = item.getText().toString();
		if(mode.getCheckedRadioButtonId() == R.id.delivery){
			mode_txt = delivery.getText().toString();
		}
		else if(mode.getCheckedRadioButtonId() == R.id.pickUp){
			mode_txt = pickUp.getText().toString();
		}
		else{
			mode_txt = meetUp.getText().toString();
		}

	    int	duration_val = duration_int[duration.getSelectedItemPosition()];
		tags_txt = tags.getText().toString();
		if(textValidator(itemName) && textValidator(description)&& textValidator(price)&&  textValidator(tags) &&textValidator(item) && !filePath.equals("")){
			//	Helper.showDialog(PostActivity.this, "Successfuly posted!");	
			getValues();

			try {
				Helper.showHorizontalProcessingDialog(PostActivity.this, null);
				post.post_menu(itemName_txt, description_txt, duration_val, item_txt, Helper.getFromSharedPref(PostActivity.this, "id"), price_txt, mode_txt, tags_txt, stores_txt, filePath);
				Helper.hideKeyboard(PostActivity.this);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}		
		else{
			Helper.showDialog(PostActivity.this, "Please check your input. All fields are required.");
		}

	}


	private void initFields(){


		thumbnail = (ImageView) findViewById(R.id.thumbnail);
		itemName = (EditText) findViewById(R.id.itemName);
		description = (EditText) findViewById(R.id.description);
		price = (EditText) findViewById(R.id.price);

		stores = (Spinner) findViewById(R.id.stores);
		item = (EditText) findViewById(R.id.item);
		duration = (Spinner)findViewById(R.id.duration) ;
		mode = (RadioGroup) findViewById(R.id.mode);
		delivery = (RadioButton) findViewById(R.id.delivery);
		pickUp = (RadioButton) findViewById(R.id.pickUp);
		meetUp = (RadioButton) findViewById(R.id.meetUp);
		tags = (EditText) findViewById(R.id.tags);
		confirm = (ImageView) findViewById(R.id.confirm);

		choosePhoto = (LinearLayout) findViewById(R.id.photoChooser);
		camera = (CustomTextView) findViewById(R.id.camera);
		gallery = (CustomTextView) findViewById(R.id.gallery);
	}

	private void getValues(){
		price.append("$");
		itemName_txt = itemName.getText().toString();
		description_txt = description.getText().toString();
		price_txt = price.getText().toString();
		stores_txt = stores.getSelectedItem().toString();
		item_txt = item.getText().toString();
		if(mode.getCheckedRadioButtonId() == R.id.delivery){
			mode_txt = delivery.getText().toString();
		}
		else if(mode.getCheckedRadioButtonId() == R.id.pickUp){
			mode_txt = pickUp.getText().toString();
		}
		else{
			mode_txt = meetUp.getText().toString();
		}
		duration_txt = duration.getSelectedItem().toString();
		tags_txt = tags.getText().toString();
	}

	private boolean textValidator(EditText text){		
		if(text.getText().toString().length() != 0)
			return true;
		else
			return false;		
	}

	private void showGallery(){
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, SELECT_PHOTO);   
	}

	private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {

		// Decode image size
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o);

		// The new size we want to scale to
		final int REQUIRED_SIZE = 140;

		// Find the correct scale value. It should be the power of 2.
		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp / 2 < REQUIRED_SIZE
					|| height_tmp / 2 < REQUIRED_SIZE) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		// Decode with inSampleSize
		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o2);

	}



	@Override
	public void onFailedPostListener(String errorResult) {
		// TODO Auto-generated method stub
		Helper.dismissDialog();
		Helper.showDialog(PostActivity.this, "ERROR RESULT");
		Log.d("POST MESSGAE ERROR", errorResult);

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent(this, FragmentTabs.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// go to previous screen when app icon in action bar is clicked
			Intent intent = new Intent(this, FragmentTabs.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}





	@Override
	public void onSuccessPostListener(int responseCode, String message) {
		Helper.dismissDialog();
		// TODO Auto-generated method stub
		Log.d("POST MENU RESPONSE", responseCode + message);
		Helper.dismissDialog();
		Helper.showDialog(PostActivity.this, "Menu Post", "Posted successfully!", new OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(PostActivity.this, FragmentTabs.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);

			}
		}, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(PostActivity.this, FragmentTabs.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);

			}
		}, "CANCEL", "OK");


	}
}
