package com.lgk.kitchet.activities;

import android.app.ListActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;

import com.lgk.kitchet.R;
import com.lgk.kitchet.listener.FeedsListener;
import com.lgk.kitchet.sdk.Feeds;
import com.lgk.kitchet.widgets.FeedsAdapter;

import org.json.JSONArray;
import org.json.JSONException;

public class SearchResultActivity extends ListActivity implements FeedsListener {


    private ListView feedList;
    private Feeds feedsAPI;
    private JSONArray feedsArray = null;
    FeedsAdapter feedsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActionBar().setTitle("");
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setLogo(R.drawable.ic_launcher);
        setContentView(R.layout.feed_list);
        String keyword = getIntent().getExtras().getString("keyword");
        feedsAPI = new Feeds(this,keyword);
        feedsAPI.fetch(Feeds.OPTION.SEARCH);

        feedsAdapter = new FeedsAdapter(this);
        setListAdapter(feedsAdapter);
        feedList = this.getListView();
        //feedList.setDivider();



    }





    @Override
    public void onSuccessGetFeeds(int responseCode, String feeds) {

        if(responseCode == 0){

            Toast.makeText(this,"No Search Found",Toast.LENGTH_SHORT).show();
            finish();
        }
        else {

            Log.d("feeds success", feeds);
            try {
                feedsArray = new JSONArray(feeds);
                feedsAdapter.setData(feedsArray);
                feedsAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFailedGetFeeds(String errorResult) {

        Toast.makeText(this,"No Search Found",Toast.LENGTH_SHORT).show();
        finish();
        Log.d("feeds failed",errorResult);
    }
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// go to previous screen when app icon in action bar is clicked
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
