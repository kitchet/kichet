package com.lgk.kitchet.activities;

import java.util.List;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.lgk.kitchet.R;
import com.lgk.kitchet.fragments.ChatFragment;
import com.lgk.kitchet.fragments.FavoritesFragment;
import com.lgk.kitchet.fragments.HomeFragment;
import com.lgk.kitchet.fragments.KitchetFragment;
import com.lgk.kitchet.fragments.PostFragment;
import com.lgk.kitchet.listener.SearchListener;
import com.lgk.kitchet.util.GlobalVariables;
import com.lgk.kitchet.util.Helper;

import android.app.Dialog;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

public class FragmentTabs extends FragmentActivity implements SearchView.OnQueryTextListener {
	private FragmentTabHost mTabHost;
	private Typeface fontawesome;

	private SearchView mSearchView;
    Facebook facebook;
    AsyncFacebookRunner  mAsyncRunner;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		setContentView(R.layout.tabs);
		getActionBar().setTitle("");
		getActionBar().setLogo(R.drawable.kichet_logo_white);

		fontawesome = Typeface.createFromAsset(this.getAssets(), "fonts/fontawesome.ttf");
		mTabHost = (FragmentTabHost)findViewById(android.R.id.tabhost);
		mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);

		mTabHost.addTab(mTabHost.newTabSpec("home").setIndicator(getResources().getString(R.string.home)),
				HomeFragment.class, null);
		mTabHost.addTab(mTabHost.newTabSpec("favorites").setIndicator(getResources().getString(R.string.bookmark)),
				FavoritesFragment.class, null);
		mTabHost.addTab(mTabHost.newTabSpec("post").setIndicator(getResources().getString(R.string.edit)),
				PostFragment.class, null);
		mTabHost.addTab(mTabHost.newTabSpec("chat").setIndicator(getResources().getString(R.string.comment)),
				ChatFragment.class, null);
		mTabHost.addTab(mTabHost.newTabSpec("kitchet").setIndicator(getResources().getString(R.string.cutlery)),
				KitchetFragment.class, null);

		//TabHost tabhost = getTabHost();
		for(int i=0;i<mTabHost.getTabWidget().getChildCount();i++) 
		{
			//mTabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.parseColor("#E3E3E3"));
			TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
			tv.setTextColor(Color.parseColor("#E3E3E3"));
			tv.setTypeface(fontawesome);
			tv.setTextSize(25);
		}


	}

	private static FragmentTabs theInstance;

	public static FragmentTabs getInstance() {
		return FragmentTabs.theInstance;
	}

	public FragmentTabs() {
		FragmentTabs.theInstance = this;
	}
	
	public void setFragment(int id){
		mTabHost.setCurrentTab(id);
	}




	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		MenuItem searchItem = menu.findItem(R.id.action_search);
		mSearchView = (SearchView) searchItem.getActionView();
		setupSearchView(searchItem);
		return true;
	}


	private void setupSearchView(MenuItem searchItem) {

		if (isAlwaysExpanded()) {
			mSearchView.setIconifiedByDefault(false);
		} else {
			searchItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM
					| MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
		}

		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchableInfo searchableInfo = searchManager.getSearchableInfo(getComponentName());
        mSearchView.setSearchableInfo(searchableInfo);

		mSearchView.setOnQueryTextListener(this);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {

		case R.id.action_search:
			onSearchRequested();
			return true;

		case R.id.action_settings:
			logout();
            return true;
            case R.id.action_kitchet_info:
                loadKitchetInfo();
                return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}



    private void loadKitchetInfo(){
       Intent intent = new Intent(this,EditProfileActivity.class);
       startActivity(intent);
    }

	private void logout(){

		Helper.showDialog(FragmentTabs.this, "Logout", "Are you sure you want to logout?", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub

			}
		}, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				Helper.removeSharedPref(FragmentTabs.this, GlobalVariables.isLogin);
				Helper.removeSharedPref(FragmentTabs.this, GlobalVariables.id);
                Helper.removeSharedPref(FragmentTabs.this, GlobalVariables.store_name);
                Helper.removeSharedPref(FragmentTabs.this, GlobalVariables.store_tagline);
				Intent intent = new Intent(FragmentTabs.this, MainActivity.class);
				startActivity(intent);
				finish();


			}
		}, "Cancel", "OK");
	}

	@Override
	public boolean onQueryTextChange(String newText) {

		return false;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
        Intent  intent = new Intent(this,SearchResultActivity.class);
        intent.putExtra("keyword",query);

		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

		//Toast.makeText(FragmentTabs.this, query, Toast.LENGTH_SHORT).show();
		return true;
	}



	protected boolean isAlwaysExpanded() {
		return false;
	}


    private void share(String  id){
        String urlToShare = "http://168.63.253.61/kitchet_api/images/"+id;
        Log.i("Test",urlToShare);
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        // intent.putExtra(Intent.EXTRA_SUBJECT, "Foo bar"); // NB: has no effect!
        intent.putExtra(Intent.EXTRA_TEXT, urlToShare);


        // See if official Facebook app is found
        boolean facebookAppFound = false;
        List<ResolveInfo> matches = getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo info : matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook.katana")) {
                intent.setPackage(info.activityInfo.packageName);
                facebookAppFound = true;
                break;
            }
        }

        // As fallback, launch sharer.php in a browser
        if (!facebookAppFound) {
            String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + urlToShare;
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
        }
        startActivityForResult(intent, 111);

    }

    public void facebookPost(String id){
        share( id);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 111)
        {
            Intent intent = new Intent(this, FragmentTabs.class);
            startActivity(intent);
            finish();
        }
    }

    /*

    public void FacebookLoginWindow() {


        String access_token = Helper.getFromSharedPrefFB(this, "access_token");
        long expires =  Helper.getFromSharedPrefFB1(this, "access_expires");
        Log.i("Test", access_token);

        if (access_token != null) {
            facebook.setAccessToken(access_token);
        }

        if (expires != 0) {
            facebook.setAccessExpires(expires);
        }

        if (!facebook.isSessionValid()) {
            facebook.authorize(this,
                    new String[] { "publish_stream" },
                    new Facebook.DialogListener() {

                        @Override
                        public void onCancel() {
                            // Function to handle cancel event
                        }

                        @Override
                        public void onComplete(Bundle values) {
                            // Function to handle complete event
                            // Edit Preferences and update facebook acess_token
                        Helper.saveOnSharedPref(getApplicationContext(),"access_token",facebook.getAccessToken());
                        Helper.saveOnSharedPref(getApplicationContext(),"access_expires",facebook.getAccessExpires());

                            //We got the token, so we can call postOnMyWall() here.
                            postOnMyWall();

                        }

                        @Override
                        public void onError(DialogError error) {
                            // Function to handle error

                        }

                        @Override
                        public void onFacebookError(FacebookError fberror) {
                            // Function to handle Facebook errors

                        }

                    });
        }
    }

    @Override
    public void onComplete(Bundle values) {
        if (values.isEmpty())
        {
            //"skip" clicked ?
            return;
        }

        // if facebookClient.authorize(...) was successful, this runs
        // this also runs after successful post
        // after posting, "post_id" is added to the values bundle
        // I use that to differentiate between a call from
        // faceBook.authorize(...) and a call from a successful post
        // is there a better way of doing this?
        if (!values.containsKey("post_id"))
        {
            try
            {
                Bundle parameters = new Bundle();
                parameters.putString("message", "this is a test");// the message to post to the wall
                facebookClient.dialog(this, "stream.publish", parameters, this);// "stream.publish" is an API call
            }
            catch (Exception e)
            {
                // TODO: handle exception
                System.out.println(e.getMessage());
            }
        }
    }

    @Override
    public void onFacebookError(FacebookError e) {

    }

    @Override
    public void onError(DialogError e) {

    }

    @Override
    public void onCancel() {

    } */
}
