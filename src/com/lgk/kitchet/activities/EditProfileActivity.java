package com.lgk.kitchet.activities;

import org.json.JSONException;
import org.json.JSONObject;

import com.lgk.kitchet.R;
import com.lgk.kitchet.fragments.HomeFragment;
import com.lgk.kitchet.fragments.KitchetFragment;
import com.lgk.kitchet.listener.ProfileListener;
import com.lgk.kitchet.listener.UpdateProfileListener;
import com.lgk.kitchet.sdk.Profile;
import com.lgk.kitchet.sdk.UpdateProfile;
import com.lgk.kitchet.util.GlobalVariables;
import com.lgk.kitchet.util.Helper;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class EditProfileActivity extends Activity implements UpdateProfileListener, ProfileListener{

	EditText name, tagline;

	Button update;
	UpdateProfile updateProfile;
	Profile getProfile;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_kichet);

		updateProfile = new UpdateProfile();
		updateProfile.setListener(this);

		getProfile = new Profile();
		getProfile.setListener(this);

		name = (EditText) findViewById(R.id.kichet_name);
		tagline = (EditText) findViewById(R.id.kichet_tagline);

		update = (Button) findViewById(R.id.update);
		update.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(!name.getText().toString().equals("") && !tagline.getText().toString().equals("")){

					try {
						updateProfile.update_profile(Helper.getFromSharedPref(EditProfileActivity.this, GlobalVariables.id), name.getText().toString(), tagline.getText().toString());
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}



				}
				else{
					Helper.showDialog(EditProfileActivity.this, "Please check your input. All fields are required.");
				}

			}
		});


		name.setText(Helper.getFromSharedPref(EditProfileActivity.this, GlobalVariables.store_name));
		tagline.setText(Helper.getFromSharedPref(EditProfileActivity.this, GlobalVariables.store_tagline));


		getActionBar().setTitle("Edit Profile");
		getActionBar().setLogo(new ColorDrawable(android.R.color.transparent));
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// go to previous screen when app icon in action bar is clicked
			Intent intent = new Intent(this, FragmentTabs.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onSuccessProfileUpdate(int responseCode,
			String message) {
		// TODO Auto-generated method stub
		if(responseCode == 1){

			Helper.saveOnSharedPref(EditProfileActivity.this, GlobalVariables.store_tagline, tagline.getText().toString());
			Helper.saveOnSharedPref(EditProfileActivity.this, GlobalVariables.store_name, name.getText().toString());
			Toast.makeText(EditProfileActivity.this, "Updated Successfully!", Toast.LENGTH_SHORT).show();
			Intent intent = new Intent(this, FragmentTabs.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);


		}
		else{
			Helper.showDialog(EditProfileActivity.this, "User not yet registered");
		}

	}

	@Override
	public void onFailedProfileUpdate(String errorResult) {
		// TODO Auto-generated method stub
		Helper.showDialog(EditProfileActivity.this, errorResult);

	}

	@Override
	public void onSuccessGetProfileInfo(int responseCode, JSONObject result) {
		// TODO Auto-generated method stub
		if (responseCode == 1){

			try {
				Helper.saveOnSharedPref(EditProfileActivity.this, GlobalVariables.store_tagline, result.getString("store_tagline"));
				Helper.saveOnSharedPref(EditProfileActivity.this, GlobalVariables.store_name, result.getString("store_name"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			Toast.makeText(EditProfileActivity.this, "Updated Successfully!", Toast.LENGTH_SHORT).show();
			Intent intent = new Intent(this, FragmentTabs.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);

		}
		else{
			Helper.showDialog(EditProfileActivity.this, "Cannot get profile info");
		}

	}

	@Override
	public void onFailedGetProfileInfo(String errorResult) {
		Helper.showDialog(EditProfileActivity.this, "Cannot get profile info");
		// TODO Auto-generated method stub

	}

}
