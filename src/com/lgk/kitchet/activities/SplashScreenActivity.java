package com.lgk.kitchet.activities;

import com.crashlytics.android.Crashlytics;
import com.lgk.kitchet.R;
import com.lgk.kitchet.util.Helper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.Window;
import io.fabric.sdk.android.Fabric;

public class SplashScreenActivity extends ActionBarActivity {

	/** Duration of wait **/
	private final int SPLASH_DISPLAY_LENGTH = 1000;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		Fabric.with(this, new Crashlytics());
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splash);

		/* New Handler to start the Menu-Activity 
		 * and close this Splash-Screen after some seconds.*/
		new Handler().postDelayed(new Runnable(){
			@Override
			public void run() {
				/* Create an Intent that will start the Menu-Activity. */
				if(Helper.getFromSharedPref(SplashScreenActivity.this, "isLogin").equals("true")){
					Intent mainIntent = new Intent(SplashScreenActivity.this,FragmentTabs.class);
					SplashScreenActivity.this.startActivity(mainIntent);

				}
				else{
					Intent mainIntent = new Intent(SplashScreenActivity.this,MainActivity.class);
					SplashScreenActivity.this.startActivity(mainIntent);

				}

				SplashScreenActivity.this.finish();

			}
		}, SPLASH_DISPLAY_LENGTH);
	}
}