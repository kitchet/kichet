package com.lgk.kitchet.listener;

import org.json.JSONObject;

public interface UpdateProfileListener {
	
	public void onSuccessProfileUpdate(int responseCode, String message);
	public void onFailedProfileUpdate(String errorResult);

}
