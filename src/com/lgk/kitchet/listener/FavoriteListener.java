package com.lgk.kitchet.listener;

public interface FavoriteListener {
	public void onSuccessFavoriteResult(int response_code, String message);
	public void onFailedFavoriteResult(String errorResult);
}
