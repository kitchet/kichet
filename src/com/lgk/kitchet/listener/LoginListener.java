package com.lgk.kitchet.listener;

import org.json.JSONObject;

public interface LoginListener {
	
	public void onSuccessLoginResult(int responseCode, JSONObject result, String message);
	public void onFailedLoginResult(String errorResult);

}
