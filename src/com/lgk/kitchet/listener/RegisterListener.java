package com.lgk.kitchet.listener;

import org.json.JSONObject;

public interface RegisterListener {
	
	public void onSuccessRegisterResult(int responseCode, JSONObject result, String message);
	public void onFailedRegisterResult(String errorResult);

}
