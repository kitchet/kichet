package com.lgk.kitchet.listener;

import org.json.JSONObject;

public interface PostListener {
	
	public void onSuccessPostListener(int responseCode, String message);
	public void onFailedPostListener(String errorResult);

}
