package com.lgk.kitchet.listener;

public interface OrderListener {
	public void onSucessOrderResult(int response_code, String message);
	public void onFailedOrderResult(String message);
}
