package com.lgk.kitchet.listener;

import org.json.JSONObject;

public interface ProfileListener {
	
	public void onSuccessGetProfileInfo(int responseCode, JSONObject message);
	public void onFailedGetProfileInfo(String errorResult);

}
