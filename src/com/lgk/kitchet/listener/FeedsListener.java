package com.lgk.kitchet.listener;

public interface FeedsListener {
	public void onSuccessGetFeeds(int responseCode, String feeds);
	public void onFailedGetFeeds(String errorResult);
}
