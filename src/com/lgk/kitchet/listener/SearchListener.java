package com.lgk.kitchet.listener;

public interface SearchListener {
	public void onSuccessSearch(int responseCode, String feeds);
	public void onFailSearch(String errorResult);
}
