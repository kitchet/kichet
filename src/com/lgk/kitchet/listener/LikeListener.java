package com.lgk.kitchet.listener;

public interface LikeListener {
	public void onSucessLikeResult(int response_code, String message);
	public void onFailedLikeResult(String message);
}
