package com.lgk.kitchet.util;

public class GlobalVariables {
	
	public static String id = "id";
	public static String isLogin = "isLogin";
	public static String kitchetData = "";
	public static String imagePath = "http://168.63.253.61/kitchet_api/images/";

    public static String store_name="store_name";
    public static String store_tagline= "store_tagline";

    public static String avatarUrl(String userID){
        return     "http://graph.facebook.com/"+userID+"/picture?type=normal";
    }

}
