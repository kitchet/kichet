package com.lgk.kitchet.util;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.lgk.kitchet.R;

public class Helper {

	private static AlertDialog alertDialog;

	private static ProgressDialog progressDialog;
	private static boolean isDialogShowing;

	public static void showDialog(Context context, String message){

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(message)
		.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

			}
		});

		builder.create();
		builder.show();



	}

    public static void showOrderDialog(Context context, String positiveButton, String negativeButton, String message, OnClickListener action){
        LayoutInflater li = LayoutInflater.from(context);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View promptsView = li.inflate(R.layout.prompt_dialog, null);
        // set prompts.xml to alertdialog builder
        builder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.txtOrderItems);
        builder .setPositiveButton(positiveButton,action)
                .setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder.create();
        builder.show();

    }


	
	public static void showConfirmDialog(Context context, String positiveButton, String negativeButton, String message, OnClickListener action){

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(message)
		.setPositiveButton(positiveButton,action)
		.setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		builder.create();
		builder.show();

	}

	public static void showProcessingDialog(Context c, String title, String meesage){
		progressDialog = ProgressDialog.show(c, title,meesage, true);
		isDialogShowing = true;
	}

	public static void dismissDialog(){
		if(isDialogShowing)
			progressDialog.dismiss();
	}

	public static boolean textValidator(Context c, EditText text){		
		if(text.getText().toString().length() != 0)
			return true;
		else
			return false;		
	}

	public static String getFromSharedPref(Context c, String id){
		SharedPreferences prefs = c.getSharedPreferences("kichet", Context.MODE_PRIVATE); 	
		return  prefs.getString(id, "false");

	}


    public static String getFromSharedPrefFB(Context c, String id){
        SharedPreferences prefs = c.getSharedPreferences("kichet", Context.MODE_PRIVATE);
        return  prefs.getString(id, "");

    }

    public static long getFromSharedPrefFB1(Context c, String id){
        SharedPreferences prefs = c.getSharedPreferences("kichet", Context.MODE_PRIVATE);
        return  prefs.getLong(id, 0);

    }


    public static void saveOnSharedPref(Context c, String id, String value){
		SharedPreferences.Editor editor = c.getSharedPreferences("kichet", Context.MODE_PRIVATE).edit();
		editor.putString(id, value);
		editor.commit();

	}

    public static void saveOnSharedPref(Context c, String id, long value){
        SharedPreferences.Editor editor = c.getSharedPreferences("kichet", Context.MODE_PRIVATE).edit();
        editor.putLong(id, value);
        editor.commit();

    }

	public static void removeSharedPref(Context c, String id){
		SharedPreferences.Editor editor = c.getSharedPreferences("kichet", Context.MODE_PRIVATE).edit();
		editor.remove(id);
		editor.commit();

	}


	public static void showDialog(final Context c, String title, String message, OnClickListener negative, OnClickListener positive,
			String negativeLabel, String positiveLabel) {
		Builder builder = new AlertDialog.Builder(c);
		builder.setTitle(title);
		builder.setMessage(message);

		if (!TextUtils.isEmpty(negativeLabel)) {
			if (negative == null) {
				negative = new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				};
			}
			builder.setNegativeButton(negativeLabel, negative);
		}
		if (TextUtils.isEmpty(positiveLabel)) {
			positiveLabel = "OK";
		}
		if (positive == null) {
			positive = new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			};
		}
		builder.setPositiveButton(positiveLabel, positive);
		builder.setCancelable(false);

		alertDialog = builder.show();
	}

	public static void showHorizontalProcessingDialog(Context c, DialogInterface.OnClickListener cancel) {

		progressDialog = new ProgressDialog(c);
		progressDialog.setCancelable(false);
		progressDialog.setCanceledOnTouchOutside(false);
		progressDialog.setTitle("UPLOADING");
		progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressDialog.setMax(100);
		progressDialog.setIndeterminate(false);


		if (cancel == null) {
			cancel = new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			};
		}

		progressDialog.setButton(ProgressDialog.BUTTON_NEGATIVE, "CANCEL", cancel);
		progressDialog.show();
		isDialogShowing = true;
	}

	public static void updater(int progress, int totalSize){
		if (isDialogShowing) {
			progressDialog.setProgressNumberFormat(null);
			progressDialog.setMax(totalSize);
			progressDialog.setProgress(progress);
		}
	}

	public static void dismissProgressDialog() {
		if (isDialogShowing) {
			progressDialog.dismiss();
			isDialogShowing = false;
		}
	}

	public static void hideKeyboard(Context c){
		InputMethodManager imm = (InputMethodManager)c.getSystemService(
				Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS, 0);
	}


}
