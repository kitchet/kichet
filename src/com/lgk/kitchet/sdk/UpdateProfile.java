package com.lgk.kitchet.sdk;

import org.apache.http.Header;
import org.json.*;

import android.util.Log;

import com.lgk.kitchet.listener.UpdateProfileListener;
import com.loopj.android.http.*;

public class UpdateProfile {

	private static UpdateProfileListener callback;

	public void setListener(UpdateProfileListener _callback) {
		callback = _callback;
	}

	public static void update_profile(String user_id, String kichet_name, String tagline) throws JSONException {
		final RequestParams params=new RequestParams();
		params.put("user_id", user_id);
		params.put("store_name", kichet_name);
		params.put("store_tagline", tagline);

        Log.i("Test",params.toString());
		KitchetRestClient.get("update_kitchet", params, new AsyncHttpResponseHandler() {

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				// TODO Auto-generated method stub

				try {
					String s = new String(arg2);
					JSONObject c = new JSONObject(s);
					int response_code = c.getInt("response_code"); 
					String message = c.getString("message");
					callback.onSuccessProfileUpdate(response_code, message);		
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


			}

			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
				// TODO Auto-generated method stub
				String s = new String(arg2);
				callback.onFailedProfileUpdate(s);

			}
		});

	}
}