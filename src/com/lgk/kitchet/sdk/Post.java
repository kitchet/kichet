package com.lgk.kitchet.sdk;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.http.Header;
import org.json.*;

import android.util.Log;

import com.lgk.kitchet.listener.LoginListener;
import com.lgk.kitchet.listener.PostListener;
import com.lgk.kitchet.util.Helper;
import com.loopj.android.http.*;

public class Post {

	private static PostListener callback;

	public void setListener(PostListener _callback) {
		callback = _callback;
	}

	public static void post_menu(String menu_name, String description, int time_duration, String total_item, String user_id, String price, String delivery_mode, String tags, String store, String photo) throws JSONException {
		final RequestParams params=new RequestParams();
		params.put("menu_name", menu_name);
		params.put("description", description);
		params.put("time_duration", getCurrentTimeStamp(time_duration));
		params.put("total_item", total_item);
		params.put("user_id", user_id);
		params.put("price", price);
		params.put("delivery_mode", delivery_mode);
		params.put("tags", tags);
		params.put("store", store);
		try {
			params.put("photo", new File(photo));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		Log.d("PARAMS", params.toString());
		KitchetRestClient.post("upload_menu", params, new AsyncHttpResponseHandler() {

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				// TODO Auto-generated method stub
				Helper.dismissProgressDialog();
				String s = new String(arg2);
				Log.d("POST MENU RESPONSE","RESPONSE"+s);

				try {

					JSONObject c = new JSONObject(s);
					int response_code = c.getInt("response_code"); 
					callback.onSuccessPostListener(response_code, s);	

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


			}

			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
				// TODO Auto-generated method stub
				String s = new String(arg2);
				Log.d("POST MENU ERROR","RESPONSE"+s);
				callback.onFailedPostListener(s);
				Helper.dismissProgressDialog();

			}

			@Override
			public void onProgress(int bytesWritten, int totalSize) {
				// TODO Auto-generated method stub
				super.onProgress(bytesWritten, totalSize);
				Helper.updater(bytesWritten, totalSize);

			}


		});

	}

	public static String getCurrentTimeStamp(int duration){
		try {


            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.add(Calendar.HOUR, duration);
            Date date = calendar.getTime();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String currentTimeStamp = dateFormat.format(date); // Find todays date

			return currentTimeStamp;
		} catch (Exception e) {
			e.printStackTrace();

			return null;
		}
	}
}