package com.lgk.kitchet.sdk;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.lgk.kitchet.listener.FeedsListener;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class Feeds {
	private FeedsListener callback;
	private String task;
	public static enum OPTION {
		FEEDS, FAVORITES, KITCHET,SEARCH,ORDERS
	};
	private String user_id;
	
	public Feeds(FeedsListener _callback)
	{
		this.callback = _callback;
	}
	
	public Feeds(FeedsListener _callback,String id)
	{
		this(_callback);
		user_id = id;
	}
	
	public void fetch(OPTION option)
	{

		final RequestParams params=new RequestParams();
		switch(option) {
		case FAVORITES:
			this.task = "favorites";
			params.put("user_id", user_id);
			break;
		case KITCHET:
			this.task = "kitchet";
			params.put("user_id", user_id);
			break;
		case SEARCH:
            this.task="search";
            params.put("keyword",user_id);
            break;
         case ORDERS:
            this.task = "orders";
            params.put("user_id", user_id);
            break;
        case FEEDS:
			this.task = "feeds";

		default:
		}
		Log.d("feeds user_id",user_id+" ");
		Log.d("feeds task",task+" ");
		KitchetRestClient.get(task, params, new AsyncHttpResponseHandler() {

			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2,
					Throwable arg3) {
				if(arg2!=null)
				callback.onFailedGetFeeds(new String(arg2));
				else
					callback.onFailedGetFeeds("Failed retrieving feeds");
			}

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				// TODO Auto-generated method stub
				String data = new String(arg2);
				Log.d("feed fav api",data);
				JSONObject jo;
				try {
					jo = new JSONObject(data);
					if(jo.getString("response_code").trim().equals("1")){
						callback.onSuccessGetFeeds(1, jo.getString("menu"));
					}else{
						callback.onFailedGetFeeds(jo.getString("message"));
					}
				} catch (JSONException e) {
					callback.onFailedGetFeeds("error occured");
					e.printStackTrace();
				}
				
			}
			
		});
	}


}
