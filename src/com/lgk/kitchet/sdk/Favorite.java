package com.lgk.kitchet.sdk;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.lgk.kitchet.listener.FavoriteListener;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class Favorite {
	private FavoriteListener callback;
/*	4. favorite
	param: user_id <string>
	       menu_id <string>
	       type = 1 favorite
		      0 unfavorite
	url:  http://168.63.253.61/kitchet_api/favorite
	type: POST
	failed response:
		{"response_code":0,"message":"please fill in the necessary fields","empty_fields":["user_id","kitchet_id"]}
		{"response_code":0,"message":"user not register"}
		{"response_code":0,"message":"kitchet store doesn't exist"}
	success response:
		{"response_code":1,"message":"you favorited this kitchet"}
		{"response_code":1,"message":"you unfavorited this kitchet"}	*/
	public Favorite(FavoriteListener _callback)
	{
		this.callback = _callback;
	}
	
	public void post(String user_id, String menu_id, String type)
	{
		final RequestParams params = new RequestParams();
		params.put("user_id", user_id);
		params.put("kitchet_id", menu_id);
		params.put("type_fav", type);
		Log.d("feed favorite",params.toString());
		KitchetRestClient.get("favorite", params, new AsyncHttpResponseHandler() {
			
			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				// TODO Auto-generated method stub

				try {
					String s = new String(arg2);
					JSONObject c = new JSONObject(s);
					int response_code = c.getInt("response_code"); 
					String message = c.getString("message");
					if(response_code == 1)
						callback.onSuccessFavoriteResult(response_code, message);
					else
						callback.onFailedFavoriteResult(message);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


			}

			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
				// TODO Auto-generated method stub
				String s = new String(arg2);
				callback.onFailedFavoriteResult(s);

			}
		});
	}
}
