package com.lgk.kitchet.sdk;

import org.apache.http.Header;
import org.json.*;

import android.util.Log;

import com.lgk.kitchet.listener.LoginListener;
import com.loopj.android.http.*;

public class Login {

	private static LoginListener callback;

	public void setListener(LoginListener _callback) {
		callback = _callback;
	}

	public static void login(String id) throws JSONException {
		final RequestParams params=new RequestParams();
		params.put("facebook_id", id);
		KitchetRestClient.get("login", params, new AsyncHttpResponseHandler() {

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				// TODO Auto-generated method stub
				try {
					String s = new String(arg2);
					Log.d("RESPONSE LOGIN", s);
					JSONObject c = new JSONObject(s);
					int response_code = c.getInt("response_code"); 			
					if(response_code == 0){
						callback.onSuccessLoginResult(response_code, null, "");
					}
					else{
						JSONObject user = c.getJSONObject("user");
						String message = c.getString("message");
						callback.onSuccessLoginResult(response_code, user, message);
					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


			}

			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
				// TODO Auto-generated method stub
				String s = new String(arg2);
				callback.onFailedLoginResult(s);

			}
		});

	}
}