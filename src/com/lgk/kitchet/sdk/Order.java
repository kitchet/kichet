package com.lgk.kitchet.sdk;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.lgk.kitchet.listener.OrderListener;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class Order {
	private OrderListener callback;
	
	public Order(OrderListener _callback)
	{
		callback = _callback;
	}
	
	public void post(String user_id, String menu_id,int total_item)
	{
		RequestParams params = new RequestParams();
		params.put("user_id", user_id);
		params.put("menu_id", menu_id);
        params.put("total_item",total_item);
Log.d("feed order",params.toString());
		KitchetRestClient.post("order", params, new AsyncHttpResponseHandler() {

			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2,
					Throwable arg3) {
				callback.onFailedOrderResult(new String(arg2));
			}

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				String data = new String(arg2);
				JSONObject result;
				try {
					result = new JSONObject(data);
					int error_code = result.getInt("response_code");
					if(error_code == 1)
						callback.onSucessOrderResult(error_code, result.getString("message"));
					else
						callback.onFailedOrderResult(result.getString("message"));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		});
		
	}
}
