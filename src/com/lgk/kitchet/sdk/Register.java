package com.lgk.kitchet.sdk;

import org.apache.http.Header;
import org.json.*;

import android.util.Log;

import com.lgk.kitchet.listener.LoginListener;
import com.lgk.kitchet.listener.RegisterListener;
import com.loopj.android.http.*;

public class Register {

	private static RegisterListener callback;

	public void setListener(RegisterListener _callback) {
		callback = _callback;
	}

	public static void register(String fb_id, String first_name, String last_name,
                                String email,  String mobile, String gender,
                                String country, String city,String store_name,String store_tagLine) throws JSONException {
		final RequestParams params=new RequestParams();
		params.put("facebook_id", fb_id);
		params.put("first_name", first_name);
		params.put("last_name", last_name);
		params.put("email_address", email);
		params.put("mobile_num", mobile);
		params.put("gender", gender);
		params.put("country", country);
		params.put("city", city);
        params.put("store_name", store_name);
        params.put("store_tagline", store_tagLine);
        Log.i("Test",params.toString());
		KitchetRestClient.post("register", params, new AsyncHttpResponseHandler() {

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				// TODO Auto-generated method stub

				try {
					String s = new String(arg2);
					JSONObject c = new JSONObject(s);
					JSONObject user = c.getJSONObject("user");
					int response_code = c.getInt("response_code"); 
					String message = c.getString("message");
					callback.onSuccessRegisterResult(response_code, user, message);		
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


			}

			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
				// TODO Auto-generated method stub
				String s = new String(arg2);
				callback.onFailedRegisterResult(s);

			}
		});

	}
}