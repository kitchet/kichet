package com.lgk.kitchet.sdk;

import com.loopj.android.http.*;

public class KitchetRestClient {
  private static final String BASE_URL = "http://168.63.253.61/kitchet_api/";

  private static AsyncHttpClient client = new AsyncHttpClient();


  public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {

      client.get(getAbsoluteUrl(url), params, responseHandler);
  }

  public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {

      client.post(getAbsoluteUrl(url), params, responseHandler);
  }

  private static String getAbsoluteUrl(String relativeUrl) {
      return BASE_URL + relativeUrl;
  }
}