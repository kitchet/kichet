package com.lgk.kitchet.sdk;

import android.util.Log;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.lgk.kitchet.listener.LikeListener;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class Like {
	private LikeListener callback;
	
	public Like(LikeListener _callback)
	{
		callback = _callback;
	}
	
	public void post(String user_id, String menu_id, String type)
	{
/*	param: user_id <string>
	       menu_id <string>
	       type = 1 like
		      0 unlike*/
		RequestParams params = new RequestParams();
		params.put("user_id", user_id);
		params.put("menu_id", menu_id);
		params.put("type", type);

        Log.i("Test", params.toString());
		KitchetRestClient.get("like", params, new AsyncHttpResponseHandler() {

			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2,
					Throwable arg3) {
				callback.onFailedLikeResult(new String(arg2));
			}

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				String data = new String(arg2);
				JSONObject result;
				try {
					result = new JSONObject(data);
					int error_code = result.getInt("response_code");
					if(error_code == 1)
						callback.onSucessLikeResult(error_code, result.getString("message"));
					else
						callback.onFailedLikeResult(result.getString("message"));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		});
		
	}
	
}
