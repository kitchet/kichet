package com.lgk.kitchet.sdk;

import org.apache.http.Header;
import org.json.*;

import android.util.Log;

import com.lgk.kitchet.listener.ProfileListener;
import com.loopj.android.http.*;

public class Profile {

	private static ProfileListener callback;

	public void setListener(ProfileListener _callback) {
		callback = _callback;
	}

	public static void profile(String user_id) throws JSONException {
		final RequestParams params=new RequestParams();
		params.put("user_id", user_id);

		Log.i("Test",params.toString());
		KitchetRestClient.get("get_kitchet_info", params, new AsyncHttpResponseHandler() {

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				// TODO Auto-generated method stub

				try {
					String s = new String(arg2);
					JSONObject c = new JSONObject(s);

					int response_code = c.getInt("response_code"); 

					if (response_code == 1){
						JSONObject user = c.getJSONObject("user");
						callback.onSuccessGetProfileInfo(response_code, user);	
					}
					else if(response_code == 0){
						callback.onSuccessGetProfileInfo(response_code, null);
					}


				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


			}

			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
				// TODO Auto-generated method stub
				String s = new String(arg2);
				callback.onFailedGetProfileInfo(s);

			}
		});

	}
}